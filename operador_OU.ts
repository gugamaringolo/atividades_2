//testando o operador OU - ||

namespace operador_OU
{
    let idade = 16;
    let maiorIdade = idade > 18;
    let posssuiAutorizacaoDosPais = true;
    
    let podeBeber = maiorIdade || posssuiAutorizacaoDosPais;

    console.log(podeBeber); // true
}